import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PokemonAPIService {
  constructor(private http: HttpClient) {}

  getPokemons(page): Promise<any> {
    return this.http
      .get(`https://pokeapi.co/api/v2/pokemon?offset=${page}`)
      .toPromise();
  }

  getPokemon(name): Promise<any> {
    return this.http
      .get(`https://pokeapi.co/api/v2/pokemon/${name}`)
      .toPromise();
  }
}
