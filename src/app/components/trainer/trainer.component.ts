import { Component, OnInit } from '@angular/core';
import { PokemonAPIService } from 'src/app/services/pokemonAPI/pokemon-api.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { getPokemons } from '../../utils/storage';
import { convertPokemon } from '../../utils/convertPokemon';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css'],
})
export class TrainerComponent implements OnInit {
  pokemons: Pokemon[] = [];
  pokemonToFocusOn: Pokemon = null;

  constructor(private pokeAPI: PokemonAPIService) {}

  updatePokemonFocus($event) {
    this.pokemonToFocusOn = $event;
  }

  ngOnInit() {
    const collectedPokemons = getPokemons();

    collectedPokemons.forEach(async (p) => {
      await this.pokeAPI
        .getPokemon(p)
        .then((response) => this.pokemons.push(convertPokemon(response)));
    });
  }
}
