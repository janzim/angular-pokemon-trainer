import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css'],
})
export class PokemonComponent implements OnInit {
  @Input() pokemonInfo: Pokemon;
  @Output() pokemonToFocusOn? = new EventEmitter<Pokemon>();

  constructor() {}

  onDetailsClick(e) {
    this.pokemonToFocusOn.emit(this.pokemonInfo);
  }

  ngOnInit(): void {}
}
