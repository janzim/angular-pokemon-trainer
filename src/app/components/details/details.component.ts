import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { addPokemon, getPokemons } from '../../utils/storage';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  @Input() pokemonInfo: Pokemon;
  collected: boolean = false;

  onCollectClick(e) {
    addPokemon(this.pokemonInfo.baseStats.name);
    this.collected = true;
  }

  constructor() {}

  ngOnChanges(): void {
    // disable "collect" button if user has the pokemon already
    const pokemons = getPokemons();
    if (
      this.pokemonInfo &&
      [...pokemons].includes(this.pokemonInfo.baseStats.name)
    ) {
      this.collected = true;
    } else {
      this.collected = false;
    }
  }

  ngOnInit(): void {}
}
