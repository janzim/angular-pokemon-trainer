import { Component, OnInit } from '@angular/core';
import { getTrainerName } from '../../utils/storage';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  get trainerName() {
    const value = getTrainerName();
    return value ? value.trainerName : '';
  }
}
