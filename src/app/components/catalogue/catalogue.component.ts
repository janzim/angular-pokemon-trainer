import { Component, OnInit } from '@angular/core';
import { PokemonAPIService } from 'src/app/services/pokemonAPI/pokemon-api.service';
import { Pokemon } from '../../models/pokemon.model';
import { stat } from '../../models/stat.model';
import { StaticSymbol } from '@angular/compiler';
import { convertPokemon } from '../../utils/convertPokemon';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css'],
})
export class CatalogueComponent implements OnInit {
  pokemons: Pokemon[] = [];
  pokemonToFocusOn: Pokemon = null;
  currentPage: number = 0;

  constructor(private pokeAPI: PokemonAPIService) {}

  onNextPageClick() {
    this.pokemons = [];
    this.currentPage += 20;
    this.generatePokemons();
  }

  onPreviousPageClick() {
    this.pokemons = [];
    this.currentPage -= 20;
    this.generatePokemons();
  }

  updatePokemonFocus($event) {
    this.pokemonToFocusOn = $event;
  }

  async ngOnInit() {
    this.generatePokemons();
  }

  async generatePokemons() {
    await this.pokeAPI.getPokemons(this.currentPage).then((response) =>
      response.results.forEach(async (p) => {
        const info = await this.pokeAPI.getPokemon(p.name);
        this.pokemons.push(convertPokemon(info));
      })
    );
  }
}
