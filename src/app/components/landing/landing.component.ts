import { Component, OnInit } from '@angular/core';
import { setTrainerName } from '../../utils/storage';
import { PokemonAPIService } from 'src/app/services/pokemonAPI/pokemon-api.service';
import { Router } from '@angular/router';
import { getTrainerName } from '../../utils/storage';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(15),
    ]),
  });

  get trainerName() {
    return this.loginForm.get('trainerName');
  }

  constructor(private pokeAPI: PokemonAPIService, private router: Router) {
    if (getTrainerName()) {
      this.router.navigateByUrl('/trainer');
    }
  }

  onSubmit(e) {
    setTrainerName(this.loginForm.value);
    this.router.navigateByUrl('/trainer');

    e.preventDefault();
  }

  ngOnInit(): void {}
}
