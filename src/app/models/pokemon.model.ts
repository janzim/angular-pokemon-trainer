import { stat } from './stat.model';

export interface Pokemon {
  baseStats: {
    name: string;
    image: string;
    types: string[];
    stats: stat[];
  };
  profile: {
    height: number;
    weight: number;
    abilities: string[];
    baseExperience: number;
  };
  moves: string[];
}
