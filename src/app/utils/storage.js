export const setTrainerName = (value) => {
  localStorage.setItem("credentials", JSON.stringify(value));
};

export const getTrainerName = () => {
  const storedValue = localStorage.getItem("credentials");

  if (storedValue) {
    return JSON.parse(storedValue);
  }

  return false;
};

export const addPokemon = (pokemon) => {
  const pokemons = getPokemons();
  pokemons.push(pokemon);

  localStorage.setItem("pokemons", JSON.stringify(pokemons));
};

export const getPokemons = () => {
  const storedValue = localStorage.getItem("pokemons");

  if (storedValue) {
    return JSON.parse(storedValue);
  }

  return [];
};
