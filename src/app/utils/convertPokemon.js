export const convertPokemon = (results) => {
  return {
    baseStats: {
      name: results.name,
      image: results.sprites.front_default,
      types: [...results.types.map((i) => i.type.name)],
      stats: [
        ...results.stats.map((i) => ({
          name: i.stat.name,
          baseStat: i.base_stat,
        })),
      ],
    },
    profile: {
      height: results.height,
      weight: results.weight,
      abilities: [...results.abilities.map((i) => i.ability.name)],
      baseExperience: results.base_experience,
    },
    moves: [...results.moves.map((i) => i.move.name)],
  };
};
